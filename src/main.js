import Vue from "vue";
import { registerSW } from "virtual:pwa-register";
import Vuesax from "vuesax";
import "vuesax/dist/vuesax.css";
import "material-icons/iconfont/material-icons.css";
import "vue-awesome/icons/brands/google";
import "vue-awesome/icons/brands/facebook";
import "vue-awesome/icons/brands/linkedin";
import "vue-awesome/icons/brands/twitter";
import "vue-awesome/icons/brands/github";
import "vue-awesome/icons/window-close";
import "vue-awesome/icons/redo";
import Icon from "vue-awesome/components/Icon";
import "./assets/all.css";
import router from "./router";
import App from "./App.vue";

const updateSW = registerSW({
    onNeedRefresh() {},
    onOfflineReady() {},
});
Vue.component("v-icon", Icon);
Vue.use(Vuesax);

new Vue({
    router,
    render: (h) => h(App),
}).$mount("#app");