import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
//import TheDrinks from "../components/TheDrinks.vue";

Vue.use(VueRouter);

const router = new VueRouter({
  base: "/",
  mode: "history",
  routes: [
    { path: "/", component: Home },
    { path: "/recipes", component: () => import("../views/Meals.vue") },
    { path: "/profile", component: () => import("../views/Profile.vue") },
    //{ path: "/drinks", component: TheDrinks },
    { path: "/products", component: () => import("../components/Cart.vue") },
    { path: "/dashboard", component: () => import("../views/Dashboard.vue") },
    {
      path: "/product/:id",
      name: "Product",
      component: () => import("../components/Product.vue"),
    },
  ],
});
export default router;
