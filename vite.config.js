import vue from "@vitejs/plugin-vue2";
//import {defineConfig} from "vite";
import { VitePWA } from "vite-plugin-pwa";
import Components from "unplugin-vue-components/vite"
const viteEnv = {}
Object.keys(process.env).forEach((key) => {
  if (key.startsWith(`VITE_`)) {
    viteEnv[`import.meta.env.${key}`] = process.env[key]
  }
})

export default {
  alias: {
    '@': require('path').resolve(__dirname, 'src')
  },
  define: viteEnv,
  plugins: [
    vue(),
    Components(),
    VitePWA({
      manifest: {
        name: "R0N1n-dev Vite PWA",
        short_name: "VVPWA",
        description: "Test for Vite PWA",
        theme_color: "#ffffff",
        id:"/",
        icons: [
          {
            src: "pwa-192x192.png",
            sizes: "192x192",
            type: "image/png",
          },
          {
            src: "pwa-512x512.png",
            sizes: "512x512",
            type: "image/png",
          },
          {
            src: "pwa-512x512.png",
            sizes: "512x512",
            type: "image/png",
            purpose: "any maskable",
          },
        ],
      },
    }),
  ],
}
/*export default defineConfig({
  
  /*server: {
    proxy: {
      "/api": {
        target: "https://newsdata.io",
        changeOrigin: false,
        secure: false,
        //rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },*/
 
